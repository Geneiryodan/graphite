import Vue from 'vue'
import Vuex from 'vuex'
import { api } from '@/api'
import { UTF16LE_GENERAL_CI } from 'mysql2/lib/constants/charsets'

Vue.use(Vuex)

const state = {
  token: null,
  userInfo: {
    avatar: '',
    blocks: null,
    id: null,
  },
  recentNotes: [],
  notification: {
    hide: true,
    type: null,
    message: ''
  }
}

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },

  SET_USER_INFO(state, userInfo) {
    state.userInfo = userInfo
  },

  CLEAR_USER(state, clearData) {
    state.token = clearData
    state.userInfo = clearData
  },

  UPDATE_RECENT_NOTES(state, notes) {
    state.recentNotes = notes
  },

  SET_NOTIFICATION(state, notification) {
    state.notification = notification
  }
}

export const actions = {
  updateRecentNotes({ commit, state }) {
    api
      .get('/notes', {headers: {Authorization: state.token, recent: true}})
      .then(response => {
        commit('UPDATE_RECENT_NOTES', response.data);
      })
  },

  clearNotification({commit, state}) {
    commit('SET_NOTIFICATION', {
      hide: true,
      type: null,
      message: ''
    })
  },

  setNotification({ commit }, data) {
    commit('SET_NOTIFICATION', Object.assign({
      hide: false,
      type: 1,
      message: ''
    }, data))
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
