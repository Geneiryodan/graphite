import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import { api } from './api'
import {mapState} from 'vuex'
import VueCodeHighlight from 'vue-code-highlight';
require('vue-code-highlight/themes/prism-okaidia.css')

Vue.use(VueCodeHighlight)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  computed: mapState({
      user: state => state.user,
    }),
    watch: {
      user: {
        immediate: true,
        handler() {        
          api.defaults.headers.common['Authorization'] = this.user
        },
      },
    },
  render: h => h(App)
}).$mount('#app')
