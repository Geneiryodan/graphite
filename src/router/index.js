import Vue from 'vue'
import VueRouter from 'vue-router'
import Store from '@/store/store'
import Home from '../views/Home.vue'
import ContainerNotes from '../components/ContainerNotes.vue'
import Note from '../components/Note.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: { title: 'Graphite' },
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: ContainerNotes,
            meta: { title: 'Graphite - Dashboard' },
        },
        {
            path: '/note/:title/:noteId',
            name: 'Edit Note',
            component: Note,
            meta: { title: 'Graphite - Editing ' },
            beforeEnter: (to, from, next) => {
                document.title = to.meta.title + to.params.title // Specifically add note title to normal Meta title
                next()
            },
        },
        {
            path: '/note',
            name: 'Create Note',
            component: Note,
            meta: { title: 'Graphite - Creating note' },
        },
    ],
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title // Update document titles
    next()
})

const getUser = () => {
    const token = localStorage.getItem('token')
    const userInfo = JSON.parse(localStorage.getItem('userInfo'))
    Store.commit('SET_TOKEN', token)
    Store.commit('SET_USER_INFO', userInfo)

    if (!token) {
        router.push('/')
    }
}

getUser();

export default router
