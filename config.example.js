module.exports = {
  port: 3000,
  db: {
    host     : 'db_hostname',
    user     : 'db_user',
    password : 'db_pass',
    database : 'db',
  },
  jwt: {
    secret: 'Place your secret key here' // 32 chars ftw
  },
  discord: {
    secret: 'discord_secretkey here'
  }
}
