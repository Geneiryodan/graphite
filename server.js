const mysql = require('mysql2')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const jwtSign = require('jsonwebtoken')
const http = require('http')
const config = require('./config')
const customenv = require('custom-env')
const path = require('path')
const fetch = require('node-fetch')
const OAuthClient = require('disco-oauth')
const app = express()
const port = config.port

customenv.env(process.env.NODE_ENV)

app.use(cors())
app.options('*', cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// Discord oAuth
const Client = new OAuthClient('701015571415826512', config.discord.secret)
Client.setScopes('identify')
Client.setRedirect('http://localhost:3000/login')

let userKey;

const pool = mysql.createPool(config.db)
const promisePool = pool.promise()
const saltRounds = 10 // bCrypt

// Compare input password vs hashed password
const comparePassword = async (password, hash) => await bcrypt.compare(password, hash)

const checkAuth = (req, res, next) => {
    jwtSign.verify(req.headers.authorization, config.jwt.secret, async (err, decoded) => {
        if (err) {
            return res.send({ invalidToken: true })
        } else {
            req.user = decoded.user[0] /* Set decoded token [username] on req.user */
            next()
        }
    })
}

app.get('/login', async (req, res) => { 
    let code = req.query.code;
    if(code == undefined) {
        return res.redirect('http://localhost:8080/')
    } else {
        userKey = await Client.getAccess(code).catch(console.error)
        
        if(userKey) {
            let user = await Client.getUser(userKey);
            const [userData] = await promisePool.query('INSERT INTO user_discord (uid, access_token, refresh_token, username, avatar, blocks) VALUES (?,?,?,?,?,?)', [user.id, userKey, 0, user.username, user.avatarUrl(300), 0])
            // res.redirect('http://localhost:8080/login')

            const [userDisco] = await promisePool.execute('SELECT * FROM user_discord WHERE uid=?', [user.id])
            const errorMsg = 'Authenticaton error, please try again!'
        
            if (userDisco.length >= 1) {
                if (user.username === userDisco[0].username) {
                    const token = jwtSign.sign({ user: [user.username] }, config.jwt.secret)
                    const userInfo = userDisco[0]
                    res.send({ token: token, userInfo: { avatar: userInfo.avatar, blocks: userInfo.blocks, id: userInfo.id } })                    
                } else {
                    return res.send({ error: errorMsg })
                }
            } else {
                return res.send({ error: errorMsg })
            }
        }
    }
});

// app.post('/login', async (req, res) => {
//     const [user] = await promisePool.execute('SELECT * FROM users WHERE email=?', [req.body.email])
//     const errorMsg = 'The email and/or password is incorrect!'

//     if (user.length >= 1) {
//         const matchedPass = await comparePassword(req.body.password, user[0].password)

//         if (req.body.email === user[0].email && matchedPass) {
//             const token = jwtSign.sign({ user: [req.body.email] }, config.jwt.secret)
//             const userInfo = user[0]
//             res.send({ token: token, userInfo: { avatar: userInfo.avatar, blocks: userInfo.blocks, id: userInfo.id } })
//         } else {
//             return res.send({ error: errorMsg })
//         }
//     } else {
//         return res.send({ error: errorMsg })
//     }
// }) 

app.get('/user_info', checkAuth, async (req, res) => {
    const [info] = await promisePool.query('SELECT avatar FROM users WHERE email=?', [req.user])
    res.send(info[0].avatar)
})

app.get('/notes', checkAuth, async (req, res) => {
    const [userId] = await promisePool.query('SELECT id FROM users where email=?', [req.user])
    const [notes] = await promisePool.query('SELECT * FROM notes WHERE uid=? ORDER BY date_edited DESC', [userId[0].id])
    const [notesRecent] = await promisePool.query('SELECT * FROM notes WHERE uid=? ORDER BY date_edited DESC LIMIT 5', [
        userId[0].id,
    ])
    const errorMsg = 'Woops! It looks like you have no notes.'

    if (!notes.length) {
        res.send({ error: errorMsg })
    } else if (req.headers.recent) {
        res.send(notesRecent)
    } else {
        res.send(notes)
    }
})

app.get('/blockTypes', checkAuth, async (req, res) => {
    const [TypeofBlocks] = await promisePool.query('select * FROM block_types')
    res.send(TypeofBlocks)
})

app.post('/saveBlockContent', checkAuth, async (req, res) => {
    const [getBlockId] = await promisePool.query('SELECT id FROM blocks WHERE id=?', [req.body.id])
    const [userId] = await promisePool.query('SELECT id FROM users where email=?', [req.user])

    if (getBlockId.length) {
        const [updateBlockContent] = await promisePool.query('UPDATE blocks SET content=? WHERE id=? AND uid=?', [
            req.body.content,
            req.body.id,
            userId[0].id,
        ])
        res.send('your data has been updated')
    } else {
        const [
            blockContent,
        ] = await promisePool.query('INSERT INTO blocks (id, uid, nid, type, content) VALUES (?,?,?,?,?)', [
            req.body.id,
            userId[0].id,
            req.body.noteID,
            req.body.type,
            req.body.content,
        ])
        res.send('your data hase been saved')
    }
})

app.post('/saveNewNote', checkAuth, async (req, res) => {
    const [getNoteId] = await promisePool.query('SELECT id FROM notes WHERE id=?', [req.body.id])
    const [userId] = await promisePool.query('SELECT id FROM users where email=?', [req.user])
    const [firstBlockContent] = await promisePool.query('SELECT content FROM blocks WHERE nid=?', [req.body.id])

    if (getNoteId.length) {
        const [
            updateBlockContent,
        ] = await promisePool.query('UPDATE notes SET content=?, title=?, date_edited=NOW() WHERE id=? AND uid=?', [
            firstBlockContent[0].content,
            req.body.title,
            req.body.id,
            userId[0].id,
        ])
        res.send({ hide: false, type: 2, message: 'Your note data has been updated' })
    } else {
        const [
            noteContent,
        ] = await promisePool.query(
            'INSERT INTO notes (id, uid, title, content, blocks_amt, date_created, date_edited) VALUES (?,?,?,?,?,NOW(),NOW())',
            [req.body.id, userId[0].id, req.body.title, 'You have no blocks in this note...', req.body.blocks_amt]
        )
        res.send({ hide: false, type: 2, message: 'Note and all blocks are saved succesfully' })
    }
})

app.post('/searchNotes', checkAuth, async (req, res) => {
    const [userId] = await promisePool.query('SELECT id FROM users where email=?', [req.user])
    const [notes] = await promisePool.query('SELECT title, id FROM notes WHERE uid=? AND title LIKE ?', [
        userId[0].id,
        '%' + req.body.title + '%',
    ])

    if (notes.length >= 1) {
        res.send(notes)
    }
})

app.post('/fetchNote', checkAuth, async (req, res) => {
    const [note] = await promisePool.query('SELECT * FROM notes WHERE id=?', [req.body.id])
    const [blocks] = await promisePool.query('SELECT * FROM blocks WHERE nid=?', [req.body.id])
    res.send([{ note }, { blocks }])
})

app.post('/deleteNote', checkAuth, async (req, res) => {
    const [checkNote] = await promisePool.query('SELECT * FROM notes where id=?', [req.body.id])
    const [note] = await promisePool.query('DELETE FROM notes WHERE id=?', [req.body.id])
    const [blocks] = await promisePool.query('DELETE FROM blocks WHERE nid=?', [req.body.id])
    const errorMsg = 'Something went wrong, please try again.'

    if (!checkNote.length) {
        res.send({ hide: false, type: 3, message: errorMsg })
    } else {
        res.send({ hide: false, type: 2, message: 'Your note has been deleted' })
    }
})

app.post('/deleteBlock', checkAuth, async (req, res) => {
    const [checkBlock] = await promisePool.query('SELECT * FROM blocks WHERE id=?', [req.body.id])
    const [block] = await promisePool.query('DELETE FROM blocks WHERE id=?', [req.body.id])
    const errorMsg = 'Something went wrong, please try again.'

    if (!checkBlock.length) {
        res.send({ hide: false, type: 3, message: errorMsg })
    } else {
        res.send({ hide: false, type: 2, message: 'Your block has been deleted' })
    }
})

app.listen(port, () => console.log(`Server running on ${port}!`))
